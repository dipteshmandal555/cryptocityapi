var express = require('express');
var app = express();
const cors = require('cors');
const mongoose = require('mongoose');
var bodyParser = require('body-parser')
const session = require('express-session');
const passport = require('passport');
const passportLocalMongoose = require('passport-local-mongoose');
    const userSchema=new mongoose.Schema(
        {
            username: String,
            password: String,
            email: String,
            NFTS:[],
            selled:[], 
            bought:[], 
            userlogo:"",
            imagepath:"",


        }
    )
userSchema.plugin(passportLocalMongoose);
       app.use(session({ 
            secret: "our secret.............",
            resave:false,
            saveUninitialized:false
        }))
app.use(cors())
app.use(bodyParser.json());
const User = new mongoose.model('users',userSchema);
passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

 
        app.use(passport.initialize());
        app.use(passport.session());

    mongoose.connect("mongodb://127.0.0.1:27017/cryptocity",{useNewUrlParser:true,useUnifiedTopology:true}).then(()=>{
    
    console.log("connected to MongoDB")})
    .catch(function (err){
        console.log(err)
    })
    ;

app.get("/",(req, res)=>{
    res.send({status:400});
})

app.use("/register",require("./register"))
app.use("/login",require("./login"))
app.use("/usersDetails",require("./usersDetails"))
app.use("/patchUser",require("./patch"))
app.use("/getNfts",require("./getNfts"))
app.use("/getNftsById",require("./getNftById"))
app.use("/bid",require("./bid"))
app.use("/logout",require("./logout"))


app.listen(3000)
console.log("Welcome to exprees !!")